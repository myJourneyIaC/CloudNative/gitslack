![](images/gitlab-slack.jpg)
# This project will create a repo and it will integrate with a Slack Channel

## Table of Content
- [Plan](#plan)
  - [Table of Content](#table-of-content)
  - [Step Cero (0)](#step-cero-0)
  - [Execution](#execution)
### Plan
This project will **It will create a Gitlab Repository and it will integrate after with Slack
### Step Cero (0)
Make sure you have a [Gitlab Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), and [Terraform](https://terraform.io) configured:

### Configurable Variables that you must set:
* **gitlab_url** = Gitlab URL of Your main Profile
* **gitlab_token** = Your access token
* **_name_new_project_** = Name of the new project
* **_description_new_project_** = Some description
* **_main_group_id_** = Id number for the master Project 
* **_project_creator_** = email or name for the owner of the project
* **_gitlab_slack_hook_** = [Slack WebHook](https://api.slack.com/incoming-webhooks)
* **_gitlab_slack_channel_** = Name of the Slack Channel

### Execution
```
 terraform init
 terraform plan 
 terraform apply --auto-approve
```

