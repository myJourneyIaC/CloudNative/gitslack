
module "gitlab" {

   source                    = "git::https://gitlab.com/myIoC/createsourcecodemanagement.git"

   gitlab_token             =  var.gitlab_token

   _name_new_project_        = "gitSlack"
   _description_new_project_ = "Project that will create a Project and It will integrate con Slack Channel"

   _main_group_id_           = var._main_group_id_
}

resource "gitlab_service_slack" "slack" {
  project                    = module.gitlab.new_project_id
  webhook                    = var._gitlab_slack_hook_
  push_events                = true
  push_channel               = var._gitlab_slack_channel_
  pipeline_events            = true
}
