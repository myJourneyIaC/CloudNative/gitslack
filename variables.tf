
variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com/api/v4/"
}

variable "gitlab_token" {
  type    = string
}

### Create new project ####

# Name Main Project
variable "_name_new_project_" {
  type        = string
  default     = "git-slack"
  description = "Name Main Project Group"
}

# Description Main Project
variable "_description_new_project_" {
  type        = string
  default     = "Project that will create a Project and It will integrate con Slack Channel"
  description = "Description Main Project Group"
}

# Main Group of this project will be belong
variable "_main_group_id_" {
  type = string
  description = "Main Group id"
}

variable "_managed_by_apps_" {
  type = string
  description = "Main Group id"
  default = "Terraform"
}

variable "_project_creator_" {
  type = string
  description = "Name Main Project Creator"
  default = "edward.josette@gmail.com"
}

/// Slack Web Hook
variable "_gitlab_slack_hook_" {
  type = string
}

variable "_gitlab_slack_channel_" {
  type = string
  default  = "somename-team-channel"
}
